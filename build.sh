#!/bin/sh
set -exu
git clone --depth 1 --single-branch --branch v2024.04 https://github.com/u-boot/u-boot
git -C u-boot rev-parse HEAD
[ "$(git -C u-boot rev-parse HEAD)" = "25049ad560826f7dc1c4740883b0016014a59789" ]
if [ -z ${SOURCE_DATE_EPOCH:+x} ] && git -C . rev-parse 2>/dev/null; then
    export SOURCE_DATE_EPOCH="$(git log -1 --format=%ct)"
fi
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64
cp meson-g12b-bananapi-cm4-mnt-reform2.dts u-boot/arch/arm/dts/
cp meson-g12b-bananapi-cm4-mnt-pocket-reform.dts u-boot/arch/arm/dts/
patch -d u-boot -p1 < 0000-dts-makefile.patch
patch -d u-boot -p1 < 0001-usb-hub-reset.patch
# FIXME: investigate how much slower the default boot order is
#patch -d u-boot -p1 < 0002-prefer-nvme-boot.patch
cp bananapi-cm4-mnt-reform2_defconfig u-boot/configs/
cp bananapi-cm4-mnt-pocket-reform_defconfig u-boot/configs/
git clone https://github.com/libreelec/amlogic-boot-fip
git -C amlogic-boot-fip reset --hard 0312a79cc65bf7bb3d66d33ad0660b66146bd36d
git -C amlogic-boot-fip clean -fdx
# MNT Reform 2
make -C u-boot bananapi-cm4-mnt-reform2_defconfig
make -C u-boot -j"$(nproc)"
mkdir amlogic-boot-fip/mnt-reform2-a311d
env --chdir=amlogic-boot-fip ./build-fip.sh bananapi-cm4io ../u-boot/u-boot.bin mnt-reform2-a311d
cp amlogic-boot-fip/mnt-reform2-a311d/u-boot.bin.sd.bin flash.bin
# overwrite random bytes with static content to make the build reproducble
dd if=/dev/zero of=flash.bin bs=512 count=1 conv=notrunc
printf @AML | cmp --bytes=4 --ignore-initial=0:528 - flash.bin
printf MNTREFORMAMLBOOT | dd of=flash.bin bs=512 conv=notrunc seek=1
printf @AML | cmp --bytes=4 --ignore-initial=0:492048 - flash.bin
printf MNTREFORMAMLBOOT | dd of=flash.bin bs=512 conv=notrunc seek=961
mv flash.bin meson-g12b-bananapi-cm4-mnt-reform2-flash.bin
# MNT Pocket Reform
make -C u-boot bananapi-cm4-mnt-pocket-reform_defconfig
make -C u-boot -j"$(nproc)"
mkdir amlogic-boot-fip/mnt-pocket-reform-a311d
env --chdir=amlogic-boot-fip ./build-fip.sh bananapi-cm4io ../u-boot/u-boot.bin mnt-pocket-reform-a311d
cp amlogic-boot-fip/mnt-pocket-reform-a311d/u-boot.bin.sd.bin flash.bin
# overwrite random bytes with static content to make the build reproducble
dd if=/dev/zero of=flash.bin bs=512 count=1 conv=notrunc
printf @AML | cmp --bytes=4 --ignore-initial=0:528 - flash.bin
printf MNTREFORMAMLBOOT | dd of=flash.bin bs=512 conv=notrunc seek=1
printf @AML | cmp --bytes=4 --ignore-initial=0:492048 - flash.bin
printf MNTREFORMAMLBOOT | dd of=flash.bin bs=512 conv=notrunc seek=961
mv flash.bin meson-g12b-bananapi-cm4-mnt-pocket-reform-flash.bin
